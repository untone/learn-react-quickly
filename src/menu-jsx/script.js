class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menu: []
        };
    }
    componentDidMount() {
        fetch("menu.json").then(response => response.json()).then(json => this.setState({ menu: json })).catch(error => {
            alert('Can\'t get menu data');
            console.error(error);
        });
    }
    render() {
        return React.createElement(
            'div',
            null,
            this.state.menu.map((x, i) => {
                return React.createElement(
                    'div',
                    { key: i },
                    React.createElement(Link, { label: x })
                );
            })
        );
    }
}

class Link extends React.Component {
    render() {
        const url = '/' + this.props.label.toLowerCase().trim().replace(' ', '-');
        return React.createElement(
            'div',
            null,
            React.createElement(
                'a',
                { href: url },
                this.props.label
            ),
            React.createElement('br', null)
        );
    }
}

ReactDOM.render(React.createElement(Menu, null), document.getElementById('menu'));
