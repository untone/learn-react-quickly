class Menu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menu: []
        }
    }
    componentDidMount() {
        fetch("menu.json")
            .then((response) => response.json())
            .then((json) => this.setState({ menu: json }))
            .catch((error) => {
                alert('Can\'t get menu data')
                console.error(error)
            })
    }
    render() {
        return <div>
            {this.state.menu.map((x, i) => {
                return <div key={i}>
                    <Link label={x} />
                </div>
            })}
        </div>
    }
}

class Link extends React.Component {
    render() {
        const url = '/' + this.props.label.toLowerCase().trim().replace(' ', '-')
        return <div>
            <a href={url}>{this.props.label}</a>
            <br />
        </div>
    }
}

ReactDOM.render(<Menu />, document.getElementById('menu'))
