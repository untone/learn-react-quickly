class TimerWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timeLeft: null,
            timeSet: null,
            timer: null,
            pause: null
        };
        this.startTimer = this.startTimer.bind(this);
        this.pauseTimer = this.pauseTimer.bind(this);
        this.resumeTimer = this.resumeTimer.bind(this);
        this.cancelTimer = this.cancelTimer.bind(this);
        this.resetTimer = this.resetTimer.bind(this);
        this.handleTimerElapsed = this.handleTimerElapsed.bind(this);
    }
    startTimer(timeLeft) {
        clearInterval(this.state.timer);
        let timer = setInterval(() => {
            console.log('2: Inside off setInterval');
            if (!this.state.pause) {
                var timeLeft = this.state.timeLeft - 1;
                if (timeLeft <= 0) {
                    clearInterval(timer);
                    timeLeft = 0;
                }
                this.setState({ timeLeft: timeLeft, pause: this.state.pause && timeLeft });
            }
        }, 1000);
        console.log('1: After setInterval');
        return this.setState({ timeLeft: timeLeft, timer: timer, timeSet: timeLeft });
    }
    pauseTimer() {
        if (this.state.timeLeft) {
            return this.setState({ pause: true });
        }
    }
    resumeTimer() {
        if (this.state.timeLeft) {
            return this.setState({ pause: false });
        }
    }
    cancelTimer() {
        if (this.state.timeLeft) {
            clearInterval(this.state.timer);
            return this.setState({ timeLeft: null, pause: false });
        }
    }
    resetTimer() {
        if (this.state.timeLeft) {
            return this.setState({ timeLeft: this.state.timeSet, pause: false });
        }
    }
    handleTimerElapsed() {
        console.log('do sound'); // TODO
        document.getElementById('end-of-time').play();
    }
    render() {
        var timerControlButtons;
        if (this.state.timeLeft) {
            timerControlButtons = React.createElement(
                'div',
                { className: 'btn-group', role: 'group' },
                React.createElement(ActionButton, { text: 'Pause', action: this.pauseTimer }),
                React.createElement(ActionButton, { text: 'Resume', action: this.resumeTimer }),
                React.createElement(ActionButton, { text: 'Reset', action: this.resetTimer }),
                React.createElement(ActionButton, { text: 'Cancel', action: this.cancelTimer })
            );
        }
        return React.createElement(
            'div',
            { className: 'row-fluid' },
            React.createElement(
                'h2',
                null,
                'Timer'
            ),
            React.createElement(
                'div',
                { className: 'btn-group', role: 'group' },
                React.createElement(Button, { time: '5', startTimer: this.startTimer }),
                React.createElement(Button, { time: '10', startTimer: this.startTimer }),
                React.createElement(Button, { time: '15', startTimer: this.startTimer })
            ),
            React.createElement(Timer, { timeLeft: this.state.timeLeft, pause: this.state.pause, alert: this.handleTimerElapsed }),
            React.createElement(TimerSound, null),
            timerControlButtons
        );
    }
}

class TimerSound extends React.Component {
    do() {
        document.getElementById('end-of-time').play();
    }
    render() {
        return React.createElement('audio', { id: 'end-of-time', src: 'flute_c_long_01.wav', preload: 'auto' });
    }
}

const Timer = props => {
    if (!props.timeLeft) {
        if (props.timeLeft === 0) {
            props.alert();
        }
        return React.createElement('div', null);
    }
    return React.createElement(
        'h1',
        null,
        'Time left: ',
        props.timeLeft,
        props.pause ? " (paused)" : ""
    );
};

class Button extends React.Component {
    startTimer() {
        this.props.startTimer(+this.props.time);
    }
    render() {
        return React.createElement(
            'button',
            { type: 'button', className: 'btn-default btn', onClick: this.startTimer.bind(this) },
            this.props.time,
            ' seconds'
        );
    }
}

const ActionButton = props => {
    return React.createElement(
        'button',
        { type: 'button', className: 'btn-default btn', onClick: props.action },
        props.text
    );
};

ReactDOM.render(React.createElement(TimerWrapper, null), document.getElementById('timer-app'));
